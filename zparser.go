package zparser

import (
	"fmt"
	"net/url"
	"time"
	"regexp"
	"strings"
	"errors"
)

const TIME_LAYOUT = "02/Jan/2006:15:04:05 -0700"

type Request struct {
	Ip string
	UrlParams url.Values
	UrlPath string
	Time_stamp time.Time
	UA string
}

func ParseRequest(line string, ie_regexp *regexp.Regexp, ie11_regexp *regexp.Regexp) (Request, error) {
	// 0 - find ip
	// 1 - find time
	// 2 - find parametr
	// 3 - find user agent
	state := 0
	in_time := false
	in_request := false
	in_url := false
	url_found := false
	in_ua := false
	//	var ip string
	//	var time_str string
	//	var url string
	space_count := 0
	ip_slice := make([]rune, 0, 1)
	time_slice := make([]rune, 0, 1)
	url_slice := make([]rune, 0, 1)
	ua_slice := make([]rune, 0, 1)

	for _, r := range(line) {
		if state == 0 {
			if r != ' ' {
				ip_slice = append(ip_slice, r)
			} else {
				state = 1
				continue
			}
		} else if state == 1 {
			if r == '[' {
				in_time = true
				continue
			} else if r == ']' {
				in_time = false
				state = 2
				continue
			}
			if in_time {
				time_slice = append(time_slice, r)
			}

		} else if state == 2 {

			if r == '"' && ! in_request {
				in_request = true
				continue
			}
			if in_request {
				if r == ' ' && !in_url && !url_found {
					in_url = true
					continue
				}
				if in_url {
					if r != ' ' {
						url_slice = append(url_slice, r)
					} else {
						url_found = true
						in_url = false
						continue
					}
				}
				if !in_url && url_found {
					if r == '"' {
						in_request = false
						state = 3
						continue
					}
				}
			}
		} else if state == 3 {
			if !in_ua && r == ' '{
				space_count += 1
			}
			if !in_ua && space_count == 4 && r == '"' {
				in_ua = true
				continue
			}
			if in_ua {
				if r != '"' {
					ua_slice = append(ua_slice, r)
				} else {
					state = 4
					break
				}
			}
		}
	}
	if state != 4 {
		return Request{}, errors.New("Could not parse log record")
	}
	ip_str := string(ip_slice)
	time_stamp, _ := time.Parse(TIME_LAYOUT, string(time_slice))
	parsed_url, err := url.Parse(string(url_slice))
//	fmt.Println("Parsed url: ")
//	fmt.Println(parsed_url)
	if err != nil {  // If we can not parse url return error
		fmt.Println("Ahaha some mazafaka error")
		return Request{}, err
	}
	ua_str := GetUserAgent(string(ua_slice), ie_regexp, ie11_regexp)

	query := parsed_url.RawQuery

//	fmt.Println(query)
	params , _ := url.ParseQuery(query)

//	fmt.Printf("Path: %s", parsed_url.Path)

	result := Request{ip_str, params, parsed_url.Path, time_stamp, ua_str}

//	fmt.Println(result)

	return result, nil
}

func GetUserAgent(user_agent_string string, ie_regexp *regexp.Regexp, ie11_regexp *regexp.Regexp) string {
	if strings.Contains(user_agent_string, "Chrome") {
		return "Chrome"
	} else if strings.Contains(user_agent_string, "Safari") {
		return "Safari"
	} else if strings.Contains(user_agent_string, "Firefox") {
		return "Firefox"
	} else if strings.Contains(user_agent_string, "Opera") {
		return "Opera"
	} else if check_ie_11(user_agent_string, ie11_regexp) {
		return "IE_11"
	} else if strings.Contains(user_agent_string, "MSIE") {
		return get_ie_with_version(user_agent_string, ie_regexp)
	} else {
		return "Other"
	}
}

func GetIE11Regexp() *regexp.Regexp {
	return regexp.MustCompile(`^.*Trident.*rv:11.0`)
}

func GetIERegexp() *regexp.Regexp {
	return regexp.MustCompile(`^.*MSIE (\d+).*`)
}

func check_ie_11(user_agent_string string, ie_regexp *regexp.Regexp) bool {
	results := ie_regexp.FindStringSubmatch(user_agent_string)
	if results != nil {
		return true
	} else {
		return false
	}
}

func get_ie_with_version(user_agent_string string, ie_regexp *regexp.Regexp) string {
	results := ie_regexp.FindStringSubmatch(user_agent_string)
	if results != nil {
		return fmt.Sprintf("IE_%v", results[1])
	} else {
		return "Other"
	}
}

//func main() {
//	url = ""
//}
